package com.gildedtros.aging;

import com.gildedtros.Item;

public class AgingStrategyFactory {

    /* As IAgingStrategy is a functional interface, we can use lambdas to implement this instead of local classes.
     * (Potential) drawbacks:
     *  - Implemented this way, aging strategies can have no state.
     *  - A lack of sharing code.
     * However, shared code seems to be minimal due to the variety in different strategies.
     * NormalAging and SmellyAging could potentially be the same strategy with different state.
     *
     * A benefit of of lambdas is that they're definitely flyweight by design.
     *
     * EDIT: the acceptance tests show that I misinterpreted GoodWine:
     * it increases at a double rate as well after the sellIn date has passed. As a result, it becomes more interesting
     * to share code and state using a class.
     *
     * We could even force passAging and legendaryAging in the same hierarchy, but
     * it feels a bit forced. (minQuality and maxQuality properties, and a getBaseDecayRate(Item) that can be overriden
     * would bring us a long way, but not completely: sellIn doesn't change for legendary items e.g.)
     */
    protected final static IAgingStrategy goodWineAging = new TwoRateAging(-1);

    protected final static IAgingStrategy legendaryAging = (item) -> { };

    protected final static IAgingStrategy passAging = (item) -> {
        int qualityRate = 1;

        if(item.sellIn > 10){
            qualityRate = 1;
        } else if (item.sellIn > 5) {
            qualityRate = 2;
        } else if (item.sellIn > 0) {
            qualityRate = 3;
        } else {
            qualityRate = -item.quality;
        }

        item.sellIn -=1;
        item.quality = Math.min(50, Math.max(0, item.quality + qualityRate));
    };

    protected final static IAgingStrategy smellyAging = new TwoRateAging(2);

    protected final static IAgingStrategy standardAging = new TwoRateAging(1);

    /**
     * This factory method picks the right IAgingStrategy for a given item.
     * @param item The item we need an IAgingStrategy for.
     * @return The appropriate IAgingStrategy for the given item.
     */
    public static IAgingStrategy strategyFor(Item item) {
        // In a newer java version, I would use the enhanced switch blocks.
        // It is conceptually cleaner, w.r.t. breaks, and allows for multiple case labels
        switch (item.name) {
            case "Good Wine":
                return goodWineAging;
            case "B-DAWG Keychain":
                return legendaryAging;
            case "Backstage passes for Re:Factor":
            case "Backstage passes for HAXX":
                return passAging;
            case "Duplicate Code":
            case "Long Methods":
            case "Ugly Variable Names":
                return smellyAging;
            default:
                return standardAging;
        }
    }
}
