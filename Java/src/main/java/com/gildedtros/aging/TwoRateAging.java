package com.gildedtros.aging;

import com.gildedtros.Item;

class TwoRateAging implements IAgingStrategy {

    private int decayRate = 0;

    TwoRateAging(int decayRate) {
        this.decayRate = decayRate;
    }

    @Override
    public void ageItem(Item item) {
        int actualDecay = (item.sellIn > 0)? this.decayRate : 2 * this.decayRate;

        item.sellIn -= 1;
        item.quality = Math.min(50, Math.max(0, item.quality - actualDecay));
    }
}