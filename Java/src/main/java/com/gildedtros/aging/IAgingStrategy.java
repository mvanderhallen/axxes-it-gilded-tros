package com.gildedtros.aging;

import com.gildedtros.Item;

public interface IAgingStrategy {

    /**
     * This method ages the item that was passed, potentially modifying it's sell-by and quality properties.
     * @param item The item to be aged
     */
    public void ageItem(Item item);

}
