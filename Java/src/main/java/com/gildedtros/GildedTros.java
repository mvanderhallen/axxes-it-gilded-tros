package com.gildedtros;

import com.gildedtros.aging.AgingStrategyFactory;
import com.gildedtros.aging.IAgingStrategy;

import java.util.Arrays;

class GildedTros {
    Item[] items;

    public GildedTros(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        Arrays.stream(items).forEach((item)->{
            IAgingStrategy agingStrategy = AgingStrategyFactory.strategyFor(item);
            agingStrategy.ageItem(item);
        });
    }
}