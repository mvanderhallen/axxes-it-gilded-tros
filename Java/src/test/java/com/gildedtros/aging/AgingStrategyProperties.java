package com.gildedtros.aging;

import com.gildedtros.Item;
import com.pholser.junit.quickcheck.Property;
import com.pholser.junit.quickcheck.generator.InRange;
import com.pholser.junit.quickcheck.runner.JUnitQuickcheck;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

import static org.junit.Assume.assumeThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(JUnitQuickcheck.class)
public class AgingStrategyProperties {

    // A requirement such as Smelly items degrade in Quality twice as fast as normal items could in theory be tested here,
    // but the requirement arguably doesn't *hold*.
    //
    //    @Property
    //    public void smellyItemsDegradeTwiceAsFast(@InRange(minInt = -1, maxInt = 15) int initialSell, @InRange(minInt = 0, maxInt = 50) int initialQuality) {
    //        Item smelly = new Item("foo", initialSell, initialQuality);
    //        Item normal = new Item("foo", initialSell, initialQuality);
    //        AgingStrategyFactory.smellyAging.ageItem(smelly);
    //        AgingStrategyFactory.standardAging.ageItem(normal);
    //
    //        // The requirement doesn't hold when quality values are clipped. Without this conditional, the property will fail.
    //        if (smelly.quality > 0) {
    //            assertEquals(2 * (initialQuality - normal.quality), initialQuality - smelly.quality);
    //        }
    //    }

    /**
     * Aging strategies, except for the legendary aging strategy, decrease sellIn date.
     *
     * @param initialSell    The initial sellby value, ranging -1 to 15.
     * @param initialQuality The initial quality, ranging 0 to 50.
     */
    @Property
    public void sellInDecreases(@InRange(minInt = -1, maxInt = 15) int initialSell, @InRange(minInt = 0, maxInt = 50) int initialQuality) {
        // This property does not hold for the legendary aging strategy.
        strategyDecreasesSellIn(AgingStrategyFactory.goodWineAging, "Good Wine Aging Strategy", initialSell, initialQuality);
        strategyDecreasesSellIn(AgingStrategyFactory.passAging, "Backstage pass Aging Strategy", initialSell, initialQuality);
        strategyDecreasesSellIn(AgingStrategyFactory.smellyAging, "Smelly Aging Strategy", initialSell, initialQuality);
        strategyDecreasesSellIn(AgingStrategyFactory.standardAging, "Default Aging Strategy", initialSell, initialQuality);
    }

    /**
     * Property test: Normal items age, as long as this does not cause clipping.
     *
     * @param initialSell    The initial sellby value, at least 1.
     * @param initialQuality The initial quality, between 0 and 50.
     */
    @Property
    public void qualityDecreasesWithStandardAging(@InRange(minInt = 1) int initialSell, @InRange(minInt = 0, maxInt = 50) int initialQuality) {
        Item foo = new Item("foo", initialSell, initialQuality);
        AgingStrategyFactory.standardAging.ageItem(foo);
        assertEquals(Math.max(0, initialQuality - 1), foo.quality);
    }

    /**
     * Property test: Normal items age twice as fast when they are spoilt, as long as this does not cause clipping.
     *
     * @param initialSell    The initial sellby value, at most 0.
     * @param initialQuality The initial quality, between 0 and 50.
     */
    @Property
    public void spoiltDecreasesTwiceAsFast(@InRange(maxInt = 0) int initialSell, @InRange(minInt = 0, maxInt = 50) int initialQuality) {
        Item foo = new Item("foo", initialSell, initialQuality);
        AgingStrategyFactory.standardAging.ageItem(foo);
        assertEquals(Math.max(0, initialQuality - 2), foo.quality);
    }

    /**
     * Property test: smelly items deteriorate quality with age twice as fast, as long as this does not cause clipping.
     *
     * @param initialSell    The initial sellby value, at least 1.
     * @param initialQuality The initial quality, between 0 and 50.
     */
    @Property
    public void smellyItemsDoublyDecrease(@InRange(minInt = 1) int initialSell, @InRange(minInt = 0, maxInt = 50) int initialQuality) {
        Item foo = new Item("foo", initialSell, initialQuality);
        AgingStrategyFactory.smellyAging.ageItem(foo);
        assertEquals(Math.max(0, initialQuality - 2), foo.quality);
    }

    /**
     * Property test: smelly items deteriorate quality with age four times as fast when spoilt, as long as this does not cause clipping.
     *
     * @param initialSell    The initial sellby value, at most 0.
     * @param initialQuality The initial quality, between 0 and 50.
     */
    @Property
    public void spoiltSmellyItemsDecreaseEvenFaster(@InRange(maxInt = 0) int initialSell, @InRange(minInt = 0, maxInt = 50) int initialQuality) {
        Item foo = new Item("foo", initialSell, initialQuality);
        AgingStrategyFactory.smellyAging.ageItem(foo);
        assertEquals(Math.max(0, initialQuality - 4), foo.quality);
    }

    /**
     * Property test: Good wine increases quality with age, as long as this does not cause clipping.
     *
     * @param initialSell    The initial sellby value, at least 1.
     * @param initialQuality The initial quality, between 0 and 50.
     */
    @Property
    public void goodWineIncreasesQuality(@InRange(minInt = 1) int initialSell, @InRange(minInt = 0, maxInt = 50) int initialQuality) {
        Item foo = new Item("foo", initialSell, initialQuality);
        AgingStrategyFactory.goodWineAging.ageItem(foo);
        assertEquals(Math.min(50, initialQuality + 1), foo.quality);
    }

    /**
     * Property test: Good wine increases quality with age at a double rate when its past its sellIn date,
     * as long as this does not cause clipping.
     *
     * @param initialSell    The initial sellby value, at most 0.
     * @param initialQuality The initial quality, between 0 and 50.
     */
    @Property
    public void goodWineIncreasesQualityDoubleRate(@InRange(maxInt = 0) int initialSell, @InRange(minInt = 0, maxInt = 50) int initialQuality) {
        Item foo = new Item("foo", initialSell, initialQuality);
        AgingStrategyFactory.goodWineAging.ageItem(foo);
        assertEquals(Math.min(50, initialQuality + 2), foo.quality);
    }

    /**
     * No strategy increases quality past 50.
     *
     * @param initialSell    The initial sellIn value, between -1 and 15.
     * @param initialQuality The initial quality value, between 0 and 50.
     */
    @Property
    public void qualityNeverIncreasedPast50(@InRange(minInt = -1, maxInt = 15) int initialSell, @InRange(minInt = 0, maxInt = 50) int initialQuality) {
        /* Using functional interfaces, the IAgingStrategy's don't carry name information, which would be interesting in the tests.
         * We could make IAgingStrategy a non-functional interface, adding a getName. In this case, we cannot implement them using lambdas
         * but instead should use (arguably, Local) Classes. It would probably be a good idea to make these classes flyweights,
         * to prevent continuous creation of new objects.
         */

        // I first code so that the method was called 5 times in 5 lines, and duplicated that for 'qualityNeverDecreasesPast0'.
        // By the time I thought of the noStrategyChangesNames property, I disliked these 5 hardcoded and duplicated lines,
        // so introduced this additional abstraction. Re. insight in my thought process, I probably should have committed the
        // file in that state, but I forgot. Sorry.
        checkForallAgingStrategies(this::strategyDoesNotIncreasePast50, initialSell, initialQuality);
    }

    /**
     * No strategy decreases quality below 0.
     *
     * @param initialSell    The initial sellIn value, between -1 and 15.
     * @param initialQuality The initial quality value, between 0 and 50.
     */
    @Property
    public void qualityNeverDecreasesPast0(@InRange(minInt = -1, maxInt = 15) int initialSell, @InRange(minInt = 0, maxInt = 50) int initialQuality) {
        checkForallAgingStrategies(this::strategyDoesNotDecreasePast0, initialSell, initialQuality);
    }

    /**
     * No strategy changes names of items.
     *
     * @param initialSell    The initial sellIn value, between -1 and 15.
     * @param initialQuality The initial quality value, between 0 and 50.
     */
    @Property
    public void noStrategyChangesName(@InRange(minInt = -1, maxInt = 15) int initialSell, @InRange(minInt = 0, maxInt = 50) int initialQuality) {
        checkForallAgingStrategies(this::strategyLeavesNameIntact, initialSell, initialQuality);
    }

    /**
     * The legendary Agingstrategy makes no changes to items.
     *
     * @param initialSell    The initial sellIn value, between -1 and 15.
     * @param initialQuality The initial quality value, between 0 and 100.
     */
    @Property
    public void legendaryNeverChanges(@InRange(minInt = -1, maxInt = 15) int initialSell, @InRange(minInt = -1, maxInt = 100) int initialQuality) {
        Item foo = new Item("foo", initialSell, initialQuality);
        AgingStrategyFactory.legendaryAging.ageItem(foo);
        assertEquals("foo", foo.name);
        assertEquals(initialSell, foo.sellIn);
        assertEquals(initialQuality, foo.quality);
    }

    /**
     * The passAging strategy increases in value slowly, but not past 50, early on.
     * @param initialSell    The initial sellIn value, at least 11.
     * @param initialQuality The initial quality value.
     */
    @Property
    public void earlyPassAging(@InRange(minInt = 11) int initialSell, @InRange(minInt = 0, maxInt = 50) int initialQuality){
        Item earlyPass = new Item("foo", initialSell, initialQuality);
        AgingStrategyFactory.passAging.ageItem(earlyPass);
        assertEquals(initialSell - 1, earlyPass.sellIn);
        assertEquals(Math.min(50, initialQuality + 1), earlyPass.quality);
    }

    /**
     * The passAging strategy increases in value more rapidly, but not past 50, as conference approaches.
     * @param initialSell    The initial sellIn value, between 6 and 10.
     * @param initialQuality The initial quality value.
     */
    @Property
    public void regularPassAging(@InRange(minInt = 6, maxInt = 10) int initialSell, @InRange(minInt = 0, maxInt = 50) int initialQuality){
        Item regularPass = new Item("foo", initialSell, initialQuality);
        AgingStrategyFactory.passAging.ageItem(regularPass);
        assertEquals(initialSell - 1, regularPass.sellIn);
        assertEquals(Math.min(50, initialQuality + 2), regularPass.quality);
    }

    /**
     * The passAging strategy increases in value most rapidly, but not past 50, as the conference is really close.
     * @param initialSell    The initial sellIn value, at least 11.
     * @param initialQuality The initial quality value.
     */
    @Property
    public void latePassAging(@InRange(minInt = 1, maxInt = 5) int initialSell, @InRange(minInt = 0, maxInt = 50) int initialQuality){
        Item latePass = new Item("foo", initialSell, initialQuality);
        AgingStrategyFactory.passAging.ageItem(latePass);
        assertEquals(initialSell - 1, latePass.sellIn);
        assertEquals(Math.min(50, initialQuality + 3), latePass.quality);
    }

    /**
     * The passAging strategy drops value to 0 after the conference has passed.
     * @param initialSell    The initial sellIn value,
     * @param initialQuality The initial quality value.
     */
    @Property
    public void conferenceFinishedPassAging(@InRange(maxInt = 0) int initialSell, @InRange(minInt = 0, maxInt = 49) int initialQuality){
        Item latePass = new Item("foo", initialSell, initialQuality);
        AgingStrategyFactory.passAging.ageItem(latePass);
        assertEquals(initialSell - 1, latePass.sellIn);
        assertEquals(0, latePass.quality);
    }

    /**
     * The checkForallAgingStrategies method can check a property for all existing aging strategies. Properties should accept a
     * strategy, strategy name, initial sellIn value and initial quality value for the item being checked.
     * It reduces duplication and provides a single source of truth for what the 'full list of strategies' is.
     *
     * @param check          The IStrategyCheck property that should be checked for the given strategy.
     * @param initialSell    The initial sellIn value that should be used for the item.
     * @param initialQuality The initial quality value that should be used for the item.
     */
    private void checkForallAgingStrategies(IStrategyCheck check, int initialSell, int initialQuality) {
        check.checkStrategy(AgingStrategyFactory.goodWineAging, "Good Wine Aging Strategy", initialSell, initialQuality);
        check.checkStrategy(AgingStrategyFactory.legendaryAging, "Legendary Aging Strategy", initialSell, initialQuality);
        check.checkStrategy(AgingStrategyFactory.passAging, "Backstage pass Aging Strategy", initialSell, initialQuality);
        check.checkStrategy(AgingStrategyFactory.smellyAging, "Smelly Aging Strategy", initialSell, initialQuality);
        check.checkStrategy(AgingStrategyFactory.standardAging, "Default Aging Strategy", initialSell, initialQuality);
    }

    /**
     * Checks that the given IAgingStrategy does not increase an item called 'foo' past a quality of 50. Note that the check
     * depends on the quality being *incremented*. If the initial quality exceeds 50, the check automatically succeeds.
     *
     * @param strategy       The IAgingStrategy to check.
     * @param strategyName   The name of the IAgingStrategy.
     * @param initialSell    The initial sellIn value.
     * @param initialQuality The initial quality value.
     */
    private void strategyDoesNotIncreasePast50(IAgingStrategy strategy, String strategyName, int initialSell, int initialQuality) {
        assumeThat(initialQuality, lessThanOrEqualTo(50));
        Item foo = new Item("foo", initialSell, initialQuality);
        strategy.ageItem(foo);
        assertTrue(foo.quality <= 50, "Quality should not increase past 50 in strategy: " + strategyName);
    }

    /**
     * Checks that the given IAgingStrategy does not decrease an item called 'foo' past a quality of 0. Note that the check
     * depends on the quality being *decremented*. If the initial quality is below 0, the check automatically succeeds.
     *
     * @param strategy       The IAgingStrategy to check.
     * @param strategyName   The name of the IAgingStrategy.
     * @param initialSell    The initial sellIn value.
     * @param initialQuality The initial quality value.
     */
    private void strategyDoesNotDecreasePast0(IAgingStrategy strategy, String strategyName, int initialSell, int initialQuality) {
        assumeThat(initialQuality, greaterThanOrEqualTo(0));
        Item foo = new Item("foo", initialSell, initialQuality);
        strategy.ageItem(foo);
        assertTrue(foo.quality >= 0, "Quality should not decrease past 0 in strategy: " + strategyName);
    }

    /**
     * Checks that the given IAgingStrategy does not change the name of an Item named 'foo'. This check does assume that
     * the name is irrelevant for the behavior of the aging strategy.
     *
     * @param strategy       The IAgingStrategy to check.
     * @param strategyName   The name of the IAgingStrategy.
     * @param initialSell    The initial sellIn value.
     * @param initialQuality The initial quality value.
     */
    private void strategyLeavesNameIntact(IAgingStrategy strategy, String strategyName, int initialSell, int initialQuality) {
        Item foo = new Item("foo", initialSell, initialQuality);
        strategy.ageItem(foo);
        assertTrue(foo.name.equals("foo"), "Name should not be changed by strategy: " + strategyName);
    }

    /**
     * Checks that the given IAgingStrategy decreases the sellIn value by exactly one.
     *
     * @param strategy       The IAgingStrategy to check.
     * @param strategyName   The name of the IAgingStrategy.
     * @param initialSell    The initial sellIn value.
     * @param initialQuality The initial quality value.
     */
    private void strategyDecreasesSellIn(IAgingStrategy strategy, String strategyName, int initialSell, int initialQuality) {
        Item foo = new Item("foo", initialSell, initialQuality);
        strategy.ageItem(foo);
        assertTrue(foo.sellIn == (initialSell - 1), "Sell-In was not decremented by exactly one by strategy:" + strategyName);
    }

    /**
     * A (private) interface that encapsulates what it means to be a check of an IAgingStrategy property; it means the method
     * should accept an IAgingStrategy to check, a name for the strategy, an initial sellIn value and an initial quality value.
     */
    private interface IStrategyCheck {
        public void checkStrategy(IAgingStrategy strategy, String strategyName, int initialSell, int initialQuality);
    }
}
