package com.gildedtros;

import org.approvaltests.Approvals;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;


import java.util.ArrayList;
import java.util.Arrays;

public class TexttestFixture {
    public static void main(String[] args) {
        System.out.println("AXXES CODE KATA - GILDED TROS");

        Item[] items = new Item[]{
                new Item("Ring of Cleansening Code", 10, 20),
                new Item("Ring of Cleansening Code", -1, 20),
                new Item("Good Wine", 2, 0),
                new Item("Elixir of the SOLID", 5, 7),
                new Item("B-DAWG Keychain", 0, 80),
                new Item("B-DAWG Keychain", -1, 80),
                new Item("Backstage passes for Re:Factor", 15, 20),
                new Item("Backstage passes for Re:Factor", 10, 49),
                new Item("Backstage passes for HAXX", 5, 49),
                new Item("Backstage passes for HAXX", 0, 30),
                new Item("test", 1, -1),
                // these smelly items do not work properly yet
                new Item("Duplicate Code", 3, 6),
                new Item("Long Methods", 3, 6),
                new Item("Ugly Variable Names", 3, 6)
        };

        GildedTros app = new GildedTros(items);

        int days = 2;
        if (args.length > 0) {
            days = Integer.parseInt(args[0]) + 1;
        }

        for (int i = 0; i < days; i++) {
            System.out.println("-------- day " + i + " --------");
            System.out.println("name, sellIn, quality");
            for (Item item : items) {
                System.out.println(item);
            }
            System.out.println();
            app.updateQuality();
        }
    }

    /**
     * This acceptance test compares with the legacy code on correctly initialized items
     */
    @Test
    public void testStock() {
        System.out.println("AXXES CODE KATA - TEST STOCK");

        Item[] items = new Item[]{
                // Regular items
                new Item("Ring of Cleansening Code", 10, 20),
                new Item("Ring of Cleansening Code", -1, 20),
                new Item("Elixir of the SOLID", 5, 7),

                //Good Wine
                new Item("Good Wine", 2, 0),

                //Legendary Items
                new Item("B-DAWG Keychain", 0, 80),
                new Item("B-DAWG Keychain", -1, 80),

                //Passes
                new Item("Backstage passes for Re:Factor", 15, 20),
                new Item("Backstage passes for Re:Factor", 13, 49), // not above 50 in slow rate
                new Item("Backstage passes for Re:Factor", 10, 49), // not above 50 in medium rate
                new Item("Backstage passes for HAXX", 5, 49),       // not above 50 in fast rate
                new Item("Backstage passes for HAXX", 0, 30),
                };

        ArrayList<String[]> stockDays = processItemsForDays(items, 20);
        Approvals.verifyAll("Day", stockDays);
    }

    /* Note that this acceptance test fails, as it tests compliance with the old legacy code on items which per requirements
     * should not exist. I'm granting myself some leeway here to change the behavior on these items.
     */

    /**
     * This acceptance test compares with the legacy code on faulty initialized items
     */
    @Disabled("Acceptance test for legacy code's behavior of invalidly configured items")
    @Test
    public void faultyTestStock() {
        System.out.println("AXXES CODE KATA - FAULTY TEST STOCK");

        Item[] items = new Item[]{
                new Item("B-DAWG Keychain", 1, 79),
                // Legendary item that isn't of quality 80
                new Item("Elixir of the SOLID", 5, -1),
                // regular item with negative value
                new Item("Elixir of the SOLID", 5, 55),
                // regular item with quality > 50
                new Item("Good Wine", 2, 52)
                // good wine with quality > 50

        };

        ArrayList<String[]> stockDays = processItemsForDays(items, 20);
        Approvals.verifyAll("Day", stockDays);
    }

    private ArrayList<String[]> processItemsForDays(Item[] items, int days) {
        GildedTros app = new GildedTros(items);

        ArrayList<String[]> stockDays = new ArrayList<String[]>();

        for (int i = 0; i < days; i++) {
            stockDays.add(Arrays.stream(app.items).map((item)->{return item.toString();}).toArray(String[]::new));
            app.updateQuality();
        }

        return stockDays;
    }
}
