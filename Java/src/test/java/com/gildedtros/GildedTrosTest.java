package com.gildedtros;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedTrosTest {

    @Test
    void foo() {
        Item[] items = new Item[] { new Item("foo", 0, 0) };
        GildedTros app = new GildedTros(items);
        app.updateQuality();
        assertEquals("foo", app.items[0].name);
    }

    @Test
    void fooDecreasesValue(){
        Item foo = new Item("foo", 10, 2);

        updateWithItems(new Item[]{foo});

        assertEqualsItem(new Item("foo", 9, 1), foo);
    }

    /* Examining and testing the legacy code for its current behavior shows that Items *can* be initialized to negative.
     * As the sellIn and quality fields of Item are public, as is the constructor, this is hard to fix without changing Item
     */
    @Test
    void qualityNeverDecreasesToNegative(){
        Item foo = new Item("foo", 10, 0);

        updateWithItems(new Item[]{foo});

        assertEqualsItem(new Item("foo", 9, 0), foo);
    }

    @Test
    void fooRapidlyDecreasesValueAfterSellIn(){
        Item foo = new Item("foo", 0, 2);

        updateWithItems(new Item[]{foo});

        assertEqualsItem(new Item("foo", -1, 0), foo);
    }

    @Test
    void qualityNeverDecreasesToNegativeEvenWhenSpoilt(){
        Item foo = new Item("foo", 0, 1);

        updateWithItems(new Item[]{foo});

        assertEqualsItem(new Item("foo", -1, 0), foo);
    }

    @Test
    void goodWineIncreasesValue() {
        Item goodWine = new Item("Good Wine", 10, 5);

        updateWithItems(new Item[]{goodWine});

        assertEqualsItem(new Item("Good Wine", 9, 6), goodWine);
    }

    /**
     * Good wine not only increases quality when aging, it also increases quality at a double rate when past its sellIn date.
     */
    @Test
    void goodWineIncreasesValueDoubleRate() {
        Item goodWine = new Item("Good Wine", 0, 5);

        updateWithItems(new Item[]{goodWine});

        assertEqualsItem(new Item("Good Wine", -1, 7), goodWine);
    }

    /* For the following tests, I test the changed behavior on their border values. However, the behavior should remain
     * the same for all values between the edge cases. In a later version, it could be interesting to use quickcheck tests
     * instead.
     */
    @Test
    void earlyPassIncreasesValue(){
        Item earlyPass = new Item("Backstage passes for Re:Factor", 11, 30);

        updateWithItems(new Item[]{earlyPass});

        assertEqualsItem(new Item("Backstage passes for Re:Factor", 10, 31), earlyPass);
    }

    @Test
    void regularPassIncreasesValue(){
        Item regularPass  = new Item("Backstage passes for Re:Factor", 10, 30);

        updateWithItems(new Item[]{regularPass});

        assertEqualsItem(new Item("Backstage passes for Re:Factor", 9, 32), regularPass);
    }

    @Test
    void latePassIncreasesValue(){
        Item latePass = new Item("Backstage passes for Re:Factor", 5, 30);

        updateWithItems(new Item[]{latePass});

        assertEqualsItem(new Item("Backstage passes for Re:Factor", 4, 33), latePass);
    }

    @Test
    void backstagePassAfterConference(){
        Item backstagePass = new Item("Backstage passes for Re:Factor", 1, 30);
        GildedTros app = new GildedTros(new Item[]{backstagePass});

        app.updateQuality();
        assertEqualsItem(new Item("Backstage passes for Re:Factor",0,33), backstagePass);

        app.updateQuality();
        assertEqualsItem(new Item("Backstage passes for Re:Factor",-1,0), backstagePass);

        app.updateQuality();
        assertEqualsItem(new Item("Backstage passes for Re:Factor",-2,0), backstagePass);
    }


    /*
     * This test tests the requirement that "The Quality of an item is never more than 50", later on made more precise
     * by stating "an item can never have its Quality increase above 50".
     * Strictly speaking, items such as Backstage Passes can increase in steps > 1, so if a backstage pass with quality
     * 49 ages and close enough to its sell-by date ages it either breaks the 'age in steps of three' requirement or the
     * "never increase quality above 50" requirement. Examining and testing the legacy code for its current behavior
     * seems like the best option here. 'Compliance' with the legacy code behavior is a good candidate for some type of
     * 'golden file' testing.
     */
    @Test
    void increaseCappedTo50() {
        // All items in this test are designed to either be at quality-cap already or to (try to) exceed quality-cap
        Item[] items = new Item[]{new Item("Good Wine", 2, 49),
                                  new Item("Good Wine", 2, 50),
                                  new Item("Backstage passes for Re:Factor", 2, 47),
                                  new Item("Backstage passes for Re:Factor", 2, 49),
                                  new Item("Backstage passes for Re:Factor", 2, 50)
        };

        updateWithItems(items);

        Arrays.stream(items).forEach((item) -> {
            assertEquals(50, item.quality);
        });
    }

    @Test
    void legendaryItemNeverChanges(){
        Item legendary = new Item("B-DAWG Keychain", 5,80);

        updateWithItems(new Item[]{legendary});

        assertEqualsItem(new Item("B-DAWG Keychain", 5,80), legendary);
    }

    @Test
    void smellyItemsDegradeFaster(){
        Item smelly = new Item("Duplicate Code", 3, 6);

        updateWithItems(new Item[]{smelly});

        assertEqualsItem(new Item("Duplicate Code", 2, 4), smelly);
    }

    @Test
    void smellyItemsDoubleDegrade(){
        Item smelly = new Item("Duplicate Code", 0, 6);

        updateWithItems(new Item[]{smelly});

        assertEqualsItem(new Item("Duplicate Code", -1, 2), smelly);
    }

    @Test
    void smellyItemsNotPastZero(){
        Item smelly = new Item("Duplicate Code", 1, 1);

        updateWithItems(new Item[]{smelly});

        assertEqualsItem(new Item("Duplicate Code", 0, 0), smelly);
    }

    @Test
    void smellyItemsNotPastZeroEvenWhenSpoilt(){
        Item smelly = new Item("Duplicate Code", 0, 1);

        updateWithItems(new Item[]{smelly});

        assertEqualsItem(new Item("Duplicate Code", -1, 0), smelly);
    }

    private void assertEqualsItem(Item expected, Item actual){
        assertEquals(expected.name, actual.name);
        assertEquals(expected.sellIn, actual.sellIn);
        assertEquals(expected.quality, actual.quality);
    }

    private void updateWithItems(Item[] items) {
        GildedTros app = new GildedTros(items);
        app.updateQuality();
    }
}
